﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Flocking {
	public class SetPriority : MonoBehaviour {
		[SerializeField] private ProcessPriorityClass m_Priority;

		private void Awake() {
			Process.GetCurrentProcess().PriorityClass = m_Priority;
		}
	}
}