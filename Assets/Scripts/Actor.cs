﻿using UnityEngine;

namespace Flocking {
	public class Actor : PooledObject {
		private float m_Speed;
		private int m_UpdateTurn;
		private RVector3 m_GoalPos;
		private Quaternion m_CurrentRotation;
		private Vector3 m_CurrentPosition;
		private int m_Group;

		private static int m_ActorCount; // Used in Start to initialize m_UpdateTurn.

		private void Start() {
			m_Group = Random.Range(0, GlobalFlock.Instance.GoalPositions.Length - 1);
			Material mat = RandomMaterial.Instance.m_Selection[m_Group];
			GetComponent<Renderer>().material = mat;

			//ParticleSystem.MainModule main = GetComponent<ParticleSystem>().main;
			//Color emissionColor = mat.GetColor("_EmissionColor");
			//emissionColor.a = 1f;
			//main.startColor = emissionColor;

			m_ActorCount++;

			m_UpdateTurn = m_ActorCount % 5;

			m_GoalPos = GlobalFlock.Instance.GoalPositions[m_Group];

			m_CurrentPosition = transform.position;
			m_CurrentRotation = transform.rotation;
		}

		public void UpdateActor(float deltaTime, System.Random random) {
			// From https://www.youtube.com/watch?v=eMpI1eCsIyM
			// Uncommented because she didn't comment it either and I don't understand any part of this code.
			// She did explain it but I don't get any of it, really.

			if (GlobalFlock.Instance.Field.Contains(m_CurrentPosition)) {
				if (++m_UpdateTurn >= 5) {
					m_UpdateTurn = 0;
					Vector3 center = Vector3.zero;
					Vector3 avoid = Vector3.zero;

					float groupSpeed = 0.1f;
					int groupSize = 0;

					Vector3 otherPosition;
					foreach (Actor actor in GlobalFlock.Instance.AllActors) {
						if (!ReferenceEquals(actor, this)) {
							otherPosition = actor.m_CurrentPosition;
							// Use the squared distance, instead of Vector3.Distance, to save time.
							// All distances in FlockingSettings are squared in Awake.
							Vector3 diffVec = new Vector3(m_CurrentPosition.x - otherPosition.x,
														  m_CurrentPosition.y - otherPosition.y,
														  m_CurrentPosition.z - otherPosition.z);
							float sqrDistToOther = diffVec.x * diffVec.x + diffVec.y * diffVec.y + diffVec.z * diffVec.z;

							if (sqrDistToOther < FlockingSettings.SqrCohesionDistance) {
								center += otherPosition;
								groupSize++;
								if (sqrDistToOther < FlockingSettings.SqrRepulsionDistance) {
									avoid += diffVec;
								}

								groupSpeed += actor.m_Speed;
							}
						}
					}

					if (groupSize > 0) {
						m_Speed = groupSpeed / groupSize;

						Vector3 direction = center / groupSize + m_GoalPos - m_CurrentPosition + avoid - m_CurrentPosition;
						if (direction.x * direction.x + direction.y * direction.y + direction.z * direction.z > 0) {
							m_CurrentRotation = Quaternion.Slerp(m_CurrentRotation,
																 Quaternion.LookRotation(direction),
																 FlockingSettings.RotationSpeed * deltaTime);
						}
					}
				}
			} else {
				m_CurrentRotation = Quaternion.Slerp(m_CurrentRotation,
													 Quaternion.LookRotation(GlobalFlock.Instance.Field.center - m_CurrentPosition),
													 FlockingSettings.RotationSpeed * deltaTime);
				m_Speed = FlockingSettings.TargetSpeed + ((float) random.NextDouble() * 2 - 1) * FlockingSettings.MaxSpeedDeviance;
			}
		}

		private void LateUpdate() {
			transform.Translate(0, 0, Time.deltaTime * m_Speed);
			transform.rotation = m_CurrentRotation;
			m_CurrentPosition = transform.position;
		}

		public override void OnTakeFromPool(ObjectPool pool, float xPos, float yPos, float zPos, params object[] params_) {
			transform.position = new Vector3(xPos, yPos, zPos);

			transform.forward = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
			m_Speed = FlockingSettings.TargetSpeed + (Random.value * 2 - 1) * FlockingSettings.MaxSpeedDeviance; // Random.value becomes [-1, 1]
		}

		public override void OnReturnToPool(ObjectPool pool) { }
	}
}
