﻿namespace Flocking {
	public class FlockingSettings {
		public const float RotationSpeed = 4;
		public const float TargetSpeed = 4;
		public const float MaxSpeedDeviance = 1;
		public const float SqrCohesionDistance = 25;
		public const float SqrRepulsionDistance = 16;
	}
}
