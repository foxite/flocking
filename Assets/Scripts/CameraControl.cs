﻿using UnityEngine;

namespace Flocking {
	public class CameraControl : MonoBehaviour {
		[SerializeField] private Camera m_Camera;

		private Vector2 m_MouseVelocity;
		private Vector3 m_MoveVelocity;

		private void Start() {
			if (m_Camera == null) {
				m_Camera = GetComponent<Camera>();
				if (m_Camera == null) {
					Debug.LogError("CameraControl " + name + " did not have a camera assigned and no camera was found on its gameobject");
					Destroy(this);
				}
			}
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}

		private void Update() {
			// When the cursor is captured, pressing escape will release it.
			// When the cursor is not captured, clicking inside the game window will capture it.

			// Proble: Locked will prevent the mouse delta from working, and Confined will prevent the mouse delta from working if the cursor is at the edge of the screen.
			if (Cursor.lockState == CursorLockMode.Locked && Input.GetKeyDown(KeyCode.Escape)) {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			} else if (Input.GetMouseButtonDown(0)) {
				Vector3 vppoint = m_Camera.ScreenToViewportPoint(Input.mousePosition);
				if (vppoint.x >= 0 && vppoint.y >= 0 && vppoint.x <= 1 && vppoint.y <= 1) { // mousePosition is in screen
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible = true;
				}
			}

			Vector2 mouseInput = new Vector2(
				Input.GetAxis("Mouse X"),
				Input.GetAxis("Mouse Y"));
			
			if (mouseInput.magnitude > 0.1) {
				m_MouseVelocity = mouseInput;
			} else if (m_MouseVelocity.sqrMagnitude != 0) {
				m_MouseVelocity *= 0.9f;
				if (m_MouseVelocity.magnitude < 0.1) {
					m_MouseVelocity = Vector2.zero;
				}
			}

			if (Input.GetMouseButton(2)) {
				// Pan
				transform.Translate(m_MouseVelocity.x, 0, m_MouseVelocity.y, Space.Self);
			} else {
				// Look
				transform.localRotation = Quaternion.Euler(
						transform.localRotation.eulerAngles.x - m_MouseVelocity.y,
						transform.localRotation.eulerAngles.y + m_MouseVelocity.x, 0);
			}

			Vector3 moveInput = new Vector3(
					(Input.GetKey(KeyCode.D) ? 1 : 0) - (Input.GetKey(KeyCode.A) ? 1 : 0),
					Input.mouseScrollDelta.y,
					(Input.GetKey(KeyCode.W) ? 1 : 0) - (Input.GetKey(KeyCode.S) ? 1 : 0));
			moveInput *= 0.5f;

			if (moveInput.magnitude > 0.05) {
				m_MoveVelocity = moveInput;
			} else if (m_MoveVelocity.magnitude != 0) {
				m_MoveVelocity *= 0.9f;
				if (m_MoveVelocity.sqrMagnitude < 0.1) {
					m_MoveVelocity = Vector2.zero;
				}
			}
			
			transform.Translate(m_MoveVelocity.x, 0, m_MoveVelocity.z, Space.Self);
			transform.Translate(0, m_MoveVelocity.y, 0, Space.World);
		}
	}
}
