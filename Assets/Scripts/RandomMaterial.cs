﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMaterial : MonoBehaviour {
	public static RandomMaterial Instance { get; private set; }

	public List<Material> m_Selection;

	private void Awake() {
		Instance = this;
	}
}
