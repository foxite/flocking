﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Flocking {
	public class GlobalFlock : MonoBehaviour {
		public static GlobalFlock Instance { get; private set; }

		[SerializeField] private int m_ActorCount;
		[SerializeField] private Bounds m_Field;
		[SerializeField] private ObjectPool m_ActorPool;

		private System.Random m_Random;
		
		public RVector3[] GoalPositions { get; private set; }
		public Bounds Field { get; private set; }
		public Actor[] AllActors { get; private set; }

		private void Awake() {
			Instance = this;
		}

		private void Start() {
			GoalPositions = new RVector3[10];
			for (int i = 0; i < GoalPositions.Length; i++) {
				GoalPositions[i] = new RVector3(0, 0, 0);
				ChangeGoalPos(GoalPositions[i]);
			}

			Field = m_Field;

			AllActors = new Actor[m_ActorCount];
			for (int i = 0; i < m_ActorCount; i++) {
				AllActors[i] = (Actor) m_ActorPool.TakeFromPool(
						Random.Range(m_Field.min.x, m_Field.max.x),
						Random.Range(m_Field.min.y, m_Field.max.y),
						Random.Range(m_Field.min.z, m_Field.max.z));
			}
			m_Random = new System.Random();
		}

		private void Update() {
			int processors = Environment.ProcessorCount;
			Task[] tasks = new Task[processors];

			for (int i = 0; i < processors; i++) {
				ActorUpdateThreadData data = new ActorUpdateThreadData(
					AllActors.Length / processors * i,
					AllActors.Length / processors * (i + 1) - 1,
					Time.deltaTime,
					m_Random
				);
				tasks[i] = Task.Run(() => {
					for (int j = data.m_Start; j < data.m_End; j++) {
						AllActors[j].UpdateActor(data.m_Deltatime, data.m_Random);
					}
				});
			}
			Task.WaitAll(tasks);

			if (Random.Range(0, 40) == 0) {
				ChangeGoalPos(GoalPositions[Random.Range(0, GoalPositions.Length)]);
			}
		}

		private void ChangeGoalPos(RVector3 vec) {
			vec.x = Random.Range(m_Field.min.x, m_Field.max.x);
			vec.y = Random.Range(m_Field.min.y, m_Field.max.y);
			vec.z = Random.Range(m_Field.min.z, m_Field.max.z);
		}

		private void OnDrawGizmosSelected() {
			if (Application.isEditor) {
				Gizmos.color = Color.green;
				Gizmos.DrawWireCube(m_Field.center, m_Field.size);
			}
		}

		private class ActorUpdateThreadData {
			public readonly int m_Start;
			public readonly int m_End;
			public readonly float m_Deltatime;
			public readonly System.Random m_Random;

			public ActorUpdateThreadData(int m_Start, int m_End, float m_Deltatime, System.Random random) {
				this.m_Start = m_Start;
				this.m_End = m_End;
				this.m_Deltatime = m_Deltatime;
				this.m_Random = random;
			}
		}
	}

	/// <summary>
	/// Like a Vector3, except it's a reference type.
	/// </summary>
	public class RVector3 {
		public float x;
		public float y;
		public float z;

		public RVector3(float x, float y, float z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}

		// Operators copied from the Unity source code
		// They all work with RVector3s but return Vector3s.
		// https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Vector3.cs#L315
		// Adds two vectors.
		public static Vector3 operator +(RVector3 a,  Vector3 b) { return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z); }
		public static Vector3 operator +( Vector3 a, RVector3 b) { return new Vector3(b.x + a.x, b.y + a.y, b.z + a.z); }
		public static Vector3 operator +(RVector3 a, RVector3 b) { return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z); }
		// Subtracts one vector from another.
		public static Vector3 operator -(RVector3 a,  Vector3 b) { return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z); }
		public static Vector3 operator -( Vector3 a, RVector3 b) { return new Vector3(b.x - a.x, b.y - a.y, b.z - a.z); }
		public static Vector3 operator -(RVector3 a, RVector3 b) { return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z); }
		// Negates a vector.
		public static Vector3 operator -(RVector3 a) { return new Vector3(-a.x, -a.y, -a.z); }
		// Multiplies a vector by a number.
		public static Vector3 operator *(RVector3 a, float d) { return new Vector3(a.x * d, a.y * d, a.z * d); }
		// Multiplies a vector by a number.
		public static Vector3 operator *(float d, RVector3 a) { return new Vector3(a.x * d, a.y * d, a.z * d); }
		// Divides a vector by a number.
		public static Vector3 operator /(RVector3 a, float d) { return new Vector3(a.x / d, a.y / d, a.z / d); }
	}
}
